# Command Hooks

Command is a small library for running shell commands with hooks.
It allows you to run code before, after and during the execution capturing stdout and stderr. 
So stdout and stderror can be manipulated seperately and displayed during execution.



## Installation

Add this line to your application's Gemfile:

```ruby
gem 'command_hooks'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install command_hooks

## Usage

### run simple command

```ruby 

require 'command_hooks'

dir_command = CommandHooks::Command.new('echo "hello world"') 
result = dir_command.run

# output 
puts result.stdout
# exit code: 
puts result.status
 
```

### run command with stdout hook
```ruby
require 'command_hooks'

Command = CommandHooks::Command
dir_command = Command.new('echo "Hello From Stdout"' )
dir_command.working_dir = '~'

# Create the hook: 
dir_command.stdout do | line | 
    puts "STDOUT LINE: #{line}" 
end 

result = dir_command.run
puts result.status
```

### run command with all hooks
```ruby 
require 'command_hooks' 

include CommandHooks

# BASH SCRIPT
# You could use a seperate shell script for this as well. 
command_string = %Q{ 
#!/bin/bash

i="0"
while [ $i -lt 5 ]
do
# write to stdout: 
echo "sleep$i"

#write to stderr: 
>&2 echo "error$i"
sleep 1

i=$[$i+1]
done

# force an exit code higher than 0
# for demo perpose to mimic an error
exit 1 
}

# END BASH SCRIPT 

my_command = Command.new(command_string)

my_command.before do |command|
    puts 'Will start running command with input' 
    puts command
    puts '------' 
end 

my_command.stdout do | line |
    puts 'STDOUT: #{line}' 
end

my_command.stderr do |line |
    puts 'STDERR: #{line}'
end


my_command.success do | result |
    puts 'ended with succes and this is the result struct' 
    puts result 
end

my_command.error do | result |
    puts 'As we gave an exit 1 in the script'
    puts 'we expect the error hook to be called' 
    puts 'so this is working'
    puts result
end 

my_command.done do |result | 
    puts "----" 
    puts "The command has ended with status #{result.status}" 
end

# Now run the command: 
my_command.run
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake false` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/command_hooks.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

