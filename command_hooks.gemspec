# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'command_hooks/version'

Gem::Specification.new do |spec|
  spec.name          = 'command_hooks'
  spec.version       = CommandHooks::VERSION
  spec.authors       = ['Maarten Bos']
  spec.email         = ['maarten.bos.nl@gmail.nl']

  spec.summary       = 'Run shell commands with hooks'
  spec.description   = 'Implements hooks around shells command to interact with the commands input and output'
  spec.homepage      = 'http:/test.com'
  spec.license       = 'MIT'

  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'test-unit', '~> 2.2.0'
  spec.add_development_dependency 'minitest'
end
