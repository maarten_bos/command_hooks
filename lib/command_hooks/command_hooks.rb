require 'open3'

module CommandHooks
  CommandVO = Struct.new(
    :command,
    :arguments,
    :id,
    :working_dir
  )

  CommandResultVO = Struct.new(
    :status,
    :message,
    :stdout,
    :stderr,
    :execution_time,
    :start_time,
    :end_time,
    :id
  )

  class CommandNotSetException < RuntimeError
  end

  # Shell Command representation with hooks
  class Command
    attr_accessor :min_error_code, :working_dir

    BEFORE_HOOK  = :before_hook
    STDOUT_HOOK  = :stdout_hook
    STDERR_HOOK  = :stderr_hook
    DONE_HOOK    = :done_hook
    SUCCESS_HOOK = :success_hook
    ERROR_HOOK   = :error_hook

    def initialize(command)
      working_dir Dir.pwd
      init_hooks
      @min_error_code = 1
      init_command command
    end

    def run
      raise CommandNotSetException unless @command_vo

      call_before_hooks(@command_vo)
      result = execute_command
      !exit_with_error_status?(result.status) ? call_success_hooks(result) : call_error_hooks(result)
      call_done_hooks(result)
      result
    end

    def stdout
      @stdout_hooks.push Proc.new
    end

    def stderr
      @stderr_hooks.push Proc.new
    end

    def before
      @before_hooks.push Proc.new
    end

    def done
      @done_hooks.push Proc.new
    end

    def success
      @success_hooks.push Proc.new
    end

    def error
      @error_hooks.push Proc.new
    end

    private

    def exit_with_error_status?(code)
      code >= @min_error_code
    end

    def init_hooks
      hook_names = [BEFORE_HOOK, STDOUT_HOOK, STDERR_HOOK, DONE_HOOK, SUCCESS_HOOK, ERROR_HOOK]
      hook_names.each do |hook_name|
        eval('@' + hook_name.to_s.downcase + 's=[]')
      end
    end

    def execute_command
      start_time                      = Time.new
      stdin, stdout, stderr, wait_thr = Open3.popen3(@command_vo.command, chdir: @working_dir)
      stdout_thread                   = create_io_handler_thread(stdout, @stdout_hooks)
      stderr_thread                   = create_io_handler_thread(stderr, @stderr_hooks)
      [stdout_thread, stderr_thread].map(&:join)

      stdin.close
      stdout.close
      stderr.close
      exit_status = wait_thr.value.exitstatus

      result                = CommandResultVO.new
      result.status         = exit_status
      result.stdout         = stdout_thread.value
      result.stderr         = stderr_thread.value
      result.start_time     = start_time
      result.end_time       = Time.new
      result.execution_time = execution_time(start_time, result.end_time)
      result
    end

    def create_io_handler_thread(io_stream, hooks)
      output = []
      thread = Thread.new do
        until (line = io_stream.gets).nil?
          line = line.chomp
          call_hooks hooks, line
          output << line
        end
        output
      end
      thread
    end

    def init_command(command)
      if command.class == CommandVO
        @command_vo = command
        working_dir command.working_dir if command.working_dir
      else
        @command_vo = create_command_vo_for_string(command)
      end
    end

    def current_time
      Time.now.strftime('%d/%m/%Y %H:%M')
    end

    def call_before_hooks(data)
      call_hooks @before_hooks, data
    end

    def call_error_hooks(data)
      call_hooks @error_hooks, data
    end

    def call_success_hooks(data)
      call_hooks @success_hooks, data
    end

    def call_done_hooks(data)
      call_hooks @done_hooks, data
    end

    def call_stdout_hooks(data)
      call_hooks @stdout_hooks, data
    end

    def call_stderr_hooks(data)
      call_hooks @stderr_hooks, data
    end

    def call_hooks(hooks, data)
      hooks.each do |hook|
        hook.call data
      end
    end

    def execution_time(start_time, end_time)
      start_time - end_time
    end

    # TODO: move this up in source. maybe in te factory (or registry)
    def create_command_vo_for_string(command)
      @command_id        = create_id
      command_vo         = CommandVO.new
      command_vo.command = command
      command_vo.id      = "#{command.split(' ')[0]}-@command_id"
      command_vo
    end

    def create_id
      Time.now.to_i.to_s + rand(36**10).to_s(36)
    end

    def working_dir(working_dir)
      @working_dir = working_dir ? working_dir : Dir.pwd
    end
  end
end
