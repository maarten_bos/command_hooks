$LOAD_PATH.unshift('../lib/')

require 'test/unit'
require 'command_hooks'

class TestCommandHooks < Test::Unit::TestCase
  include CommandHooks

  def test_async_command
    command = Command.new './bash/test/sh'
    time    = Time.new
    command.stdout do |line|
      old_time = time
      time     = Time.new
      assert(time - old_time) < 2000
    end
  end

  def test_echo_command
    command = Command.new 'echo hello world'
    result  = command.run

    assert_instance_of CommandResultVO, result
    assert_instance_of Array, result.stdout
    assert_instance_of Array, result.stderr
    assert_instance_of Fixnum, result.status
    assert_instance_of Float, result.execution_time
    assert_instance_of Time, result.start_time
    assert_instance_of Time, result.end_time
    assert_equal result.status, 0
    assert_equal [], result.stderr
    assert_not_equal [], result.stdout
    assert_equal ['hello world'], result.stdout
  end

  def test_stderror
    command_string = "ruby #{File.expand_path(File.dirname(__FILE__))}/../bin/create_output.rb"

    command = Command.new(command_string)

    before_time = Time.now
    result      = command.run
    after_time  = Time.now

    assert_instance_of CommandResultVO, result
    assert_instance_of Array, result.stdout
    assert_instance_of Array, result.stderr
    assert_instance_of Fixnum, result.status
    assert_instance_of Float, result.execution_time
    assert_instance_of Time, result.start_time
    assert_instance_of Time, result.end_time
    assert result.end_time > result.start_time
    assert before_time < result.start_time
    assert before_time < result.end_time
    assert after_time > result.start_time
    assert after_time > result.end_time
    assert_not_equal [], result.stderr
    assert_equal result.status, 0
  end

  def test_before_hook
    command = Command.new 'ls -ltr'
    command.before do
      $BEFORE = true
    end

    command.run
    assert_equal true, $BEFORE
  end

  def test_success_hook
    command = Command.new 'ls -ltr'
    command.success do
      $BEFORE = true
    end

    command.run
    assert_equal true, $BEFORE
  end

  def test_error_hook
    command = Command.new 'ls -lrtsdfgsdfg444tr'

    test = 0
    command.error do
      test += 1
    end

    command.run
    assert_equal 1, test
  end

  def test_done_hook
    command = Command.new 'ls -ltr'

    command.done do
      $AFTER = true
    end

    command.done do |result_vo|
      result_vo.stdout = 'whooooooow'
    end

    result = command.run
    assert_equal true, $AFTER
    assert_equal result.stdout, 'whooooooow'
  end

  def test_stdout_hook
    command_string = 'for i in {1..5}; do echo $i; done'
    command        = Command.new command_string
    x              = 1
    command.stdout do |line|
      assert_equal x.to_s, line.chomp
      x += 1
    end
    result = command.run
    result
  end

  def test_stderr_hook
    command_string = "ruby #{File.expand_path(File.dirname(__FILE__))}/../bin/create_output.rb"
    command        = Command.new command_string
    command.stderr do |line|
      assert_equal line, 'error'
    end
    command.run
  end

  def test_working_dir_as_property
    command_string      = 'ls'
    command             = Command.new command_string
    command.working_dir = '/'
    result              = command.run

    assert result.stdout.include? 'sbin'
  end

  def test_working_dir_in_command_vo
    command_vo             = CommandVO.new
    command_vo.command     = 'ls'
    command_vo.working_dir = '/'
    command_vo.id          = 'test1'

    command = Command.new(command_vo)
    result = command.run
    assert result.stdout.include? 'sbin'
  end
end
