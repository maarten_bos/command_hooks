require 'test/unit'
require 'json'
$LOAD_PATH.unshift('../', '../lib/')
require 'command_hooks'
class MyTest < Test::Unit::TestCase
  include CommandHooks

  def test_command_returns_a_hash
    result = CommandResultVO.new
    assert_instance_of Hash, result.to_h
  end

  def test_command_returns_a_json
    result = CommandResultVO.new
    assert_instance_of String, result.to_json
  end
end
